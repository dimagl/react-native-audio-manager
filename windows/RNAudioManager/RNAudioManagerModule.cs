using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Audio.Manager.RNAudioManager
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNAudioManagerModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNAudioManagerModule"/>.
        /// </summary>
        internal RNAudioManagerModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNAudioManager";
            }
        }
    }
}
