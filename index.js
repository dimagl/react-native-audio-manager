import { NativeModules, NativeEventEmitter } from 'react-native';

const { RNAudioManager } = NativeModules;
const invariant = require('invariant');

const AudioManagerEmitter = new NativeEventEmitter(RNAudioManager);

let AudioManager = {
  /**
   * The `addListener` function connects a JavaScript function to an identified native
   * audio notification event.
   *
   * This function then returns the reference to the listener.
   *
   * @param {string} eventName The `nativeEvent` is the string that identifies the event you're listening for.  This
   *can be any of the following:
   *
   * - `audioSessionRouteChange`
   *
   * @param {function} callback function to be called when the event fires.
   */
  addListener(eventName: AudioEventName, callback: AudioEventEventListener) {
    invariant(false, 'Dummy method used for documentation');
  },

  /**
   * Removes a specific listener.
   *
   * @param {string} eventName The `nativeEvent` is the string that identifies the event you're listening for.
   * @param {function} callback function to be called when the event fires.
   */
  removeListener(
    eventName: AudioEventName,
    callback: AudioEventEventListener,
  ) {
    invariant(false, 'Dummy method used for documentation');
  },

  /**
   * Removes all listeners for a specific event type.
   *
   * @param {string} eventType The native event string listeners are watching which will be removed.
   */
  removeAllListeners(eventName: AudioEventName) {
    invariant(false, 'Dummy method used for documentation');
  },

  /**
   * Dismisses the active keyboard and removes focus.
   */
  sampleMethod() {
    invariant(false, 'Dummy method used for documentation');
  },
};

if (RNAudioManager) {
  AudioManager = AudioManagerEmitter;
  AudioManager.sampleMethod = RNAudioManager.sampleMethod;
}

export default AudioManager;
