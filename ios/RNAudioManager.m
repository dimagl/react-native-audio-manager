#import "RNAudioManager.h"

@implementation RNAudioManager

RCT_EXPORT_MODULE()

- (instancetype)init
{
    self = [super init];

    NSLog(@"Init");
    
    [[NSNotificationCenter defaultCenter] addObserver: self
    selector: @selector(audioSessionInterruptionEnded:)
    name: AVAudioSessionRouteChangeNotification
    object: nil];

    return self;
}

- (void)audioSessionInterruptionEnded:(NSNotification *)notification {
    
    NSDictionary *prevDevice = [notification.userInfo valueForKeyPath:AVAudioSessionRouteChangePreviousRouteKey];
    NSArray *prevOutputs = [prevDevice valueForKeyPath:@"outputs"];
    
    NSLog(@"Log: prevOutputs %@", prevOutputs);
    for (id obj in prevOutputs)
    NSLog(@"Log: prevOutputs - : %@", obj);
    
    NSString *prevUID = @"";
    if (prevOutputs != nil && [prevOutputs count] > 0) {
        NSDictionary *prevFirstOutput =  [prevOutputs objectAtIndex:0];
    
        NSLog(@"Log: prevFirstOutput %@", prevFirstOutput);
        
        prevUID = [prevFirstOutput valueForKeyPath:@"UID"];
    }
    NSLog(@"Log: prevUID: %@", prevUID);
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSArray *currentOutputs = [audioSession.currentRoute valueForKeyPath:@"outputs"];
    
    for (id obj in currentOutputs)
    NSLog(@"Log: currentOutputs - : %@", obj);
    
    NSLog(@"Log: currentOutputs %@", currentOutputs);
    NSString *currentUID = @"";
    if (currentOutputs != nil && [currentOutputs count] > 0) {
        NSDictionary *currentFirstOutput =  [currentOutputs objectAtIndex:0];
        
        NSLog(@"Log: currentFirstOutput %@", currentFirstOutput);
        
        currentUID = [currentFirstOutput valueForKeyPath:@"UID"];
    }
    NSLog(@"Log: currentUID: %@", currentUID);
    
//    if ([prevUID length] > 0 && [currentUID length] > 0)
    [self sendEventWithName:@"audioSessionRouteChange" body:@{
        @"previousUID": prevUID,
        @"currentUID": currentUID,
    }];
}

- (NSArray<NSString *> *)supportedEvents {
  return @[@"audioSessionRouteChange"];
}

- (void)startObserving {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    for (NSString *notificationName in [self supportedEvents]) {
        [center addObserver:self
               selector:@selector(emitEventInternal:)
                   name:notificationName
                 object:nil];
    }
}

- (void)stopObserving {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

RCT_EXPORT_METHOD(sampleMethod)
{
    NSLog(@"Log: sampleMethod");
    // TODO: Implement
}

@end
